package com.HopOn;

import com.HopOn.models.api.ApiInput;
import com.HopOn.models.api.ApiResult;
import com.HopOn.models.api.ApiResultGeneric;
import com.HopOn.models.api.ResultStatus;
import org.codehaus.jackson.map.*;
import org.codehaus.jackson.type.TypeReference;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import java.io.IOException;

public class HopOnApi {
    private String token;
    private String url;

    public void setToken(String token)
    {
        this.token = token;
    }

    public void setToken()
    {
        this.setToken("");
    }

    public String getToken()
    {
        return this.token;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void setUrl()
    {
        setUrl("http://localhost:33151");
    }

    public String getUrl()
    {
        return this.url;
    }

    public HopOnApi()
    {
        setToken();
        setUrl();
    }

    public HopOnApi(String token)
    {
        this.setToken(token);
        this.setUrl();
    }

    public HopOnApi(String token, String url)
    {
        this.setToken(token);
        this.setUrl(url);
    }

    private String serializeToJson(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(object);
        return json;
    }

    private <T>  ApiResultGeneric<T> deserializeJsonGeneric(String jsonData) throws IOException {
        ApiResultGeneric<T> result = new ApiResultGeneric<>();
        ObjectMapper mapper = new ObjectMapper();
        result = mapper.readValue(jsonData, result.getClass());
        return result;
    }

    private ApiResult deserializeJson(String jsonData) throws IOException {
        ApiResult result = new ApiResult();
        ObjectMapper mapper = new ObjectMapper();
        result = mapper.readValue(jsonData, result.getClass());
        return result;
    }

    private String requestPost(String data, String controller, String action) throws Exception
    {
        String url = this.getUrl() + "/api/" + controller + "/" + action;
        URL obj = new URL(url);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1",8080));

        HttpURLConnection con = (HttpURLConnection) obj.openConnection(proxy);

        // Setting basic post request
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type","application/json");
        //String postJsonData = data;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();


        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String output;
        StringBuffer response = new StringBuffer();

        while ((output = in.readLine()) != null) {
            response.append(output);
        }
        in.close();

        return response.toString();
    }

    //api methods
    public ApiResultGeneric<Boolean> amILoggedIn() {
        ApiInput input = new ApiInput();
        input.Token = this.getToken();
        ApiResultGeneric<Boolean> result;

        try {
            String json = this.serializeToJson(input);
            String resultAsJson = this.requestPost(json, "Account","AmILoggedIn");
            result = this.deserializeJsonGeneric(resultAsJson);
            return result;
        } catch (Exception e) {
            result = new ApiResultGeneric<>();
            result.Status = ResultStatus.Thrown;
            return result;
        }
    }
}
