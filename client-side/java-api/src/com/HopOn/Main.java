package com.HopOn;

import com.HopOn.models.api.ApiResultGeneric;

public class Main {

    public static void main(String[] args) {

        HopOnApi api = new HopOnApi();
        ApiResultGeneric<Boolean> result =  api.amILoggedIn();
        System.out.println(result.Data);
    }
}
