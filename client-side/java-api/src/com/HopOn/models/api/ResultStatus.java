package com.HopOn.models.api;

public enum ResultStatus {
    Successful,
    Failed,
    Thrown,
    Unauthorized,

}
