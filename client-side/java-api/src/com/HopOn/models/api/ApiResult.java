package com.HopOn.models.api;
import java.util.*;
public class ApiResult {
    public ResultStatus Status = ResultStatus.Successful;
    public ArrayList<String> Messages = new ArrayList<String>();
}
