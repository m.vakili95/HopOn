
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/08/2017 16:54:39
-- Generated from EDMX file: D:\Projects\HopOn\server-side\DAL\HopOnContext.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HopOn];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserLoginActivity]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LoginActivities] DROP CONSTRAINT [FK_UserLoginActivity];
GO
IF OBJECT_ID(N'[dbo].[FK_Service_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Service] DROP CONSTRAINT [FK_Service_inherits_User];
GO
IF OBJECT_ID(N'[dbo].[FK_Parent_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Parent] DROP CONSTRAINT [FK_Parent_inherits_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[LoginActivities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LoginActivities];
GO
IF OBJECT_ID(N'[dbo].[Users_Service]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_Service];
GO
IF OBJECT_ID(N'[dbo].[Users_Parent]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_Parent];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [IsActive] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LoginActivities'
CREATE TABLE [dbo].[LoginActivities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Token] nvarchar(max)  NOT NULL,
    [LastLoginTime] datetime  NOT NULL,
    [IsActive] bit  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'Users_Service'
CREATE TABLE [dbo].[Users_Service] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'Users_Parent'
CREATE TABLE [dbo].[Users_Parent] (
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LoginActivities'
ALTER TABLE [dbo].[LoginActivities]
ADD CONSTRAINT [PK_LoginActivities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_Service'
ALTER TABLE [dbo].[Users_Service]
ADD CONSTRAINT [PK_Users_Service]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_Parent'
ALTER TABLE [dbo].[Users_Parent]
ADD CONSTRAINT [PK_Users_Parent]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [User_Id] in table 'LoginActivities'
ALTER TABLE [dbo].[LoginActivities]
ADD CONSTRAINT [FK_UserLoginActivity]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserLoginActivity'
CREATE INDEX [IX_FK_UserLoginActivity]
ON [dbo].[LoginActivities]
    ([User_Id]);
GO

-- Creating foreign key on [Id] in table 'Users_Service'
ALTER TABLE [dbo].[Users_Service]
ADD CONSTRAINT [FK_Service_inherits_User]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Users_Parent'
ALTER TABLE [dbo].[Users_Parent]
ADD CONSTRAINT [FK_Parent_inherits_User]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------