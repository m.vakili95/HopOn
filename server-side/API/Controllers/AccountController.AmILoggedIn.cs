﻿using System.Linq;
using System.Web.Http;
using Model;
using Model.Api;

namespace API.Controllers
{
    public partial class AccountController
    {

        [HttpPost]
        public ApiResult<bool> AmILoggedIn([FromBody] ApiInput input)
        {

            return new DataJob<bool>
            {
                Do = (context, result)=>
                {
                    result.Data = context.LoginActivities.Any(u => u.Token == input.Token && u.IsActive);
                },
                Authorized = false
            }.Run();
        }

        [HttpGet]
        public ApiResult<bool> AmILoggedIn([FromBody] string token)
        {

            return new DataJob<bool>
            {
                Do = (context, result) =>
                {
                    result.Data = context.LoginActivities.Any(u => u.Token == token && u.IsActive);
                },
                Authorized = false
            }.Run();
        }

    }
}