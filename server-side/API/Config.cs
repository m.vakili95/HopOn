﻿using System.Web;

namespace API
{
    public static class Config
    {
        public static int? MyId => (int?)HttpContext.Current.Session?["UserId"];

    }
}