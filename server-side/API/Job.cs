﻿using System;
using System.Transactions;
using DAL;
using Model;
using Model.Api;
using AccountController = API.Controllers.AccountController;

namespace API
{

    public class Job<T>
    {
        public delegate void Work(ApiResult<T> result);

        public Work Do { get; set; }

        public bool Authorized { get; set; } = true;
        public string Token { get; set; } = "";


        public ApiResult<T> Run()
        {
            var result = new ApiResult<T> { Status = ResultStatus.Successful };

            try
            {
                if (Authorized)
                {
                    var res = new AccountController().AmILoggedIn(Token);
                    result.Status = res.Status;
                    result.Messages = res.Messages;
                    if (res.Status == ResultStatus.Successful && !res.Data)
                    {
                        result.Status = ResultStatus.Unauthorized;
                        return result;

                    }
                }
                Do(result);
            }
            catch (Exception)
            {
                result.Messages.Clear();
                result.Status = ResultStatus.Thrown;
            }

            return result;
        }
    }

    public class Job
    {
        public delegate void Work(ApiResult result);

        public Work Do { get; set; }
        public bool Authorized { get; set; } = true;
        public string Token { get; set; } = "";

        public ApiResult Run()
        {
            var result = new ApiResult { Status = ResultStatus.Successful };
            try
            {
                if (Authorized)
                {
                    var res = new AccountController().AmILoggedIn(Token);
                    result.Status = res.Status;
                    result.Messages = res.Messages;
                    if (res.Status == ResultStatus.Successful && !res.Data)
                    {
                        result.Status = ResultStatus.Unauthorized;
                        return result;
                    }
                }
                Do(result);
            }
            catch (Exception)
            {
                result.Messages.Clear();
                result.Status = ResultStatus.Thrown;
            }

            return result;
        }
    }
    public class DataJob<T>
    {
        public delegate void Work(HopOnContextContainer context, ApiResult<T> result);

        public Work Do { get; set; }
        public bool Authorized { get; set; } = true;
        public string Token { get; set; } = "";


        public ApiResult<T> Run()
        {
            var result = new ApiResult<T> { Status = ResultStatus.Successful };


            try
            {
                if (Authorized)
                {
                    var res = new AccountController().AmILoggedIn(Token);
                    result.Status = res.Status;
                    result.Messages = res.Messages;
                    if (res.Status == ResultStatus.Successful && !res.Data)
                    {
                        result.Status = ResultStatus.Unauthorized;
                        return result;

                    }
                }
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    var context = new HopOnContextContainer();
                    Do(context, result);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result.Messages.Clear();
                result.Status = ResultStatus.Thrown;
            }

            return result;
        }
    }

    public class DataJob
    {
        public delegate void Work(HopOnContextContainer context, ApiResult result);

        public Work Do { get; set; }
        public bool Authorized { get; set; } = true;
        public string Token { get; set; } = "";

        public ApiResult Run()
        {
            var result = new ApiResult { Status = ResultStatus.Successful };


            try
            {
                if (Authorized)
                {
                    var res = new AccountController().AmILoggedIn(Token);
                    result.Status = res.Status;
                    result.Messages = res.Messages;
                    if (res.Status == ResultStatus.Successful && !res.Data)
                    {
                        result.Status = ResultStatus.Unauthorized;
                        return result;

                    }
                }
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    var context = new HopOnContextContainer();
                    Do(context, result);
                    scope.Complete();
                }
            }
            catch (Exception)
            {
                result.Messages.Clear();
                result.Status = ResultStatus.Thrown;
            }

            return result;
        }
    }


}
