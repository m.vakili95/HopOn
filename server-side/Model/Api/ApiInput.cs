﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Api
{
    public class ApiInput
    {
        public string Token { get; set; } = "";
    }

    public class ApiInput<T> : ApiInput
    {
        public T Input { get; set; }
    }


}
