﻿using System;
using System.Collections.Generic;

namespace Model.Api
{
    public class ApiResult
    {
        public String Status { get; set; } = ResultStatus.Successful;
        public List<string> Messages { get; set; } = new List<string>();
    }

    public class ApiResult<T> : ApiResult
    {
        public T Data { get; set; }
    }
}