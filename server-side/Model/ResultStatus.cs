﻿
namespace Model
{
    public static class ResultStatus
    {
        public static string Successful => "Successful";
        public static string Failed => "Failed";
        public static string Thrown => "Thrown";
        public static string Unauthorized => "Unauthorized";

    }
}